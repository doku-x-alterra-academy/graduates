// ## Directions
// Diberikan 3 variable yang me-representasikan seorang murid:

// - nama
// - nilai
// - absen

// Buatlah algoritma/pseudocode dan implementasi sebuah kondisional yang menentukan apakah murid tersebut lulus atau tidak.

// Murid dinyatakan lulus jika:

// - nilainya 70 keatas
// - absennya dibawah 5 kali

// Tampilkan nama murid dan keterangan apakah murid tersebut 'lulus' atau 'tidak lulus'


var nama = 'jessika';
var nilai = 70;
var absen = 0;

if (nilai >= 70 && absen < 5){
    console.log(`${nama}, anda lulus`)
}else{
    console.log(`${nama}, anda tidak lulus`)
}